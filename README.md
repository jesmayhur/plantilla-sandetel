# README #

Plantillas elaboradas por Jesús Maya Hurtado, a partir del repo realizado por Rafael Bretón con las plantillas LaTex para la DGTSI de la CEEC.

### What is this repository for? ###

* Plantilla para documentos Sandetel
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* El documento principal de compilación es 0_documento_xxxx.tex. En el mismo se incluyen todos los documentos auxiliares, incluidas las partes del documento (ejemplo parte_1.tex). Habría que incluír cuantas partes, anexos, etc se creen, mediante comando include.
* En la carpeta tex se encuentran documentos relacionados que deberán modificarse o no según las circunstancia. En todo caso se aconseja reviarar las definiciones (0_definiciones.tex)



### Who do I talk to? ###

* Repo owner or admin: Jesús Maya Hurtado - jesus.maya@juntadeandalucia.es
* Other community or team contact